Database structure :
	[x] Users table. Need for /speed/ setting level-access. Super-user (can all); Moderator (can work with news and album and raspisanie).
		-SU is 1.
		-Mod is 2.

	[x] News table. There store data for news.

	[x] Albums table. There store data for albums.

	[x] Photos table. There store data for photos.

Tables and rows :

	[x] users-table :
		-id
		-login_hash
		-pass_hash
		-access_level

	[x] news-table :
		-id
		-user_id (who post it)
		-album_id
		-view_count
		-title
		-text
		-time
		-date
		-preview_photo_uid

	[x] albums-table :
		-id
		-user_id
		-view_count
		-title
		-photos_uid (for link with photos-table)
		-main_photo_id
		
	[x] photos-table :
		-id
		-uid
		-file_name_full
		-file_name_preview
		-description