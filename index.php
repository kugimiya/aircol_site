<?php

	ini_set('display_errors', 0);

	function draw_fzf()
	{
		echo "404 not found";
		exit;
	}

	function drop_in_view_handler($page_name)
	{
		$pages = scandir('res/cache/');
		$num = -1;

		for ($i=0; $i < count($pages); $i++)
			if ($page_name == $pages[$i])
				$num = $i;

		if ($num != -1)
			include "res/cache/{$pages[$num]}";
		else
			draw_fzf();
	}
	
	if (is_null($_REQUEST['page']))
		drop_in_view_handler('index');
	else
		drop_in_view_handler($_REQUEST['page']);

?>