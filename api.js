
	var error_message = "Пожалуйста, проверьте ваше соединение с Интернет. Но, возможно ведутся работы на сервере. Если данное окно появляется вновь, напишите в тех.поддержку -- bp.001@ya.ru";

	function getXmlHttp(){
	  var xmlhttp;
	  try {
	    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	  } catch (e) {
	    try {
	      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch (E) {
	      xmlhttp = false;
	    }
	  }
	  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
	    xmlhttp = new XMLHttpRequest();
	  }
	  return xmlhttp;
	}

	function add_to_page_raw_last_news(raw_response)
	{
		var json_parsed = JSON.parse(raw_response);

		if (json_parsed.response == undefined)
			alert('Что-то пошло не так...');
		else
		{
			for (var i = 0; i < json_parsed.response.length; i++) {
				//json_parsed.response[i]
				//console.log(json_parsed.response[i].preview_photo_uid);
			
				//формируем html, короче

				//здесь главный див новости
				var newNewsDiv = document.createElement('div');
				newNewsDiv.id = json_parsed.response[i].id;
				newNewsDiv.className = "news-block";
				//newNewsDiv.innerHTML = json_parsed.response.text;

				//здесь т.н. див времени публикации
				var publishTmstmpDiv = document.createElement('div');
				publishTmstmpDiv.className = "news-publication-timestamp";
				publishTmstmpDiv.innerHTML = "Опубликовано : " + json_parsed.response[i].date + " " + json_parsed.response[i].time;

				//здесь тайтл новости; т.к. моя говно-верстка, то простым текстом получится проще же
				var newsSubject = "<center><h2>" + json_parsed.response[i].title + "</center></h2>";

				//здесь т.н. контент-бокс, тут хранится картинка (если есть) и текст новости
				var contentBoxDiv = document.createElement('div');
				contentBoxDiv.className = "content-box";

				//здесь т.н. контент-бокс-имаге, тут понятно же
				//if (image_exist) .....
				var contentBoxImageDiv = document.createElement('div');
				contentBoxImageDiv.className = "content-box-image";
				contentBoxImageDiv.innerHTML = "<center><img src=/res/files/img_thumb/"+ json_parsed.response[i].preview_photo_uid +"><center>";

				//здесь текст новости
				var contentBoxTextDiv = document.createElement('div');
				contentBoxTextDiv.className = "content-box-test";
				contentBoxTextDiv.innerHTML = json_parsed.response[i].text;

				//приступим к сборке этого дива

				newNewsDiv.appendChild(publishTmstmpDiv);
				newNewsDiv.innerHTML += "<br clear=\"all\">";
				newNewsDiv.innerHTML += newsSubject;

				contentBoxDiv.appendChild(contentBoxImageDiv);
				contentBoxDiv.appendChild(contentBoxTextDiv);

				newNewsDiv.appendChild(contentBoxDiv);

				//пока NOT пропустим кнопку с альбомом
				if (json_parsed.response[i].album_id != -1)
				{
					var album_button = document.createElement('div');
					album_button.className = "next-link-box";
					album_button.innerHTML = "<a href=\"javascript:event_show_album(" + json_parsed.response[i].album_id + ");\">Галерея ></a>";

					newNewsDiv.appendChild(album_button);
				}


				//новостной див собран, прикрутим его к концу всех других новостей
				var parentDiv = document.getElementById('test');
				parentDiv.appendChild(newNewsDiv);
			};
		}
	}

	function event_get_last_news() 
	{		
		init_loaded_started = 1;
		var xhr = getXmlHttp();
		xhr.open('GET', '/api.php?req=get_last_news', true);
		xhr.send();

		xhr.onreadystatechange = function() 
		{
			if (xhr.readyState == 4)
				if (xhr.status == 200) {
					init_loaded = 1;
					add_to_page_raw_last_news(xhr.responseText);
					init_loaded_started = 0;
				}
		};

		xhr.ontimeout = function()
		{
			init_loaded_started = 0;
			alert(error_message);
		};

		xhr.onerror = function()
		{
			init_loaded_started = 0;
			alert(error_message);
		};
	}

	function event_get_last_news_by_count_id()
	{
		var news = document.getElementsByTagName('div');
		for (var i = 0; i < news.length; i++) {
			if (news[i].className == "news-block")
				var last_id = news[i].id;
		};

		if (last_id > 1)
		{
			init_loaded_started = 1;
			var xhr = getXmlHttp();
			xhr.open('GET', '/api.php?req=get_news_by_id&id='+last_id, true);
			xhr.send();

			xhr.onreadystatechange = function() 
			{
				if (xhr.readyState == 4)
					if (xhr.status == 200) {
						init_loaded = 1;
						add_to_page_raw_last_news(xhr.responseText);
						init_loaded_started = 0;
					}
			};

			xhr.ontimeout = function()
			{
				init_loaded_started = 0;
				alert(error_message);
			};

			xhr.onerror = function()
			{
				init_loaded_started = 0;
				alert(error_message);
			};
		} else {
			init_loaded = 2;
		}
	}

	function isVisible(elem) {

      var coords = elem.getBoundingClientRect();

      var windowHeight = document.documentElement.clientHeight;

      // верхняя граница elem в пределах видимости ИЛИ нижняя граница видима
      var topVisible = coords.top > 0 && coords.top < windowHeight;
      var bottomVisible = coords.bottom < windowHeight && coords.bottom > 0;

      return topVisible || bottomVisible;
    }
    
	function showVisible()
	{
		var check = document.getElementById('show_more');

		if (isVisible(check) && (init_loaded != 2)) {
          
			if (init_loaded == 1)
				if (init_loaded_started == 0)
					event_get_last_news_by_count_id();

			//if (init_loaded == 0)
				//if (init_loaded_started == 0)
					//

        }
	}

	function event_show_album(req_id)
	{
		var album = document.getElementById('album');
		album.style="display:block;";

		var xhr = getXmlHttp();
		xhr.open('GET', '/api.php?req=get_files_links_by_news_id&id='+req_id, true);
		xhr.send();

		xhr.onreadystatechange = function() 
		{
			if (xhr.readyState == 4)
				if (xhr.status == 200) {
					//add_to_page_raw_last_news(xhr.responseText);
					//album.innerHTML = xhr.responseText;

					var json_parsed = JSON.parse(xhr.responseText);

					if (json_parsed.response == undefined)
						alert('Что-то пошло не так...');
					else
					{
						var flt_bnr_img = document.createElement('div');
						flt_bnr_img.id = "flt_bnr_img";
						
						for (var i = 0; i < json_parsed.response.length; i++) {
							//json_parsed.response[i]

							var file_block = document.createElement('div');
							var img_block = document.createElement('img');

							file_block.className = "float_banner_image";

							img_block.src = "/res/files/img_thumb/"+json_parsed.response[i].file_name_preview;
							file_block.appendChild(img_block);

							flt_bnr_img.appendChild(file_block);
						}

						var album_block = document.getElementById('albumBlock');
						album_block.appendChild(flt_bnr_img);
					}
				}
		};

		xhr.ontimeout = function()
		{
			init_loaded_started = 0;
			alert(error_message);
		};

		xhr.onerror = function()
		{
			init_loaded_started = 0;
			alert(error_message);
		};
	}

	function event_close_album()
	{
		var flt_img_ban = document.getElementById('flt_bnr_img');
		flt_img_ban.innerHTML = "";
		flt_img_ban.id = "nuuuulll"; //лень гуглить, как удалять его

		var album = document.getElementById('album');
		album.style="display:none;";
	}

	var init_loaded = 0;
	var init_loaded_started = 0;
	event_get_last_news();
	window.onscroll = showVisible;