<?php
	ini_set('display_errors', 1);

	include 'admins_panel/lib/config.php';
	include 'admins_panel/lib/libdb.php';
	//include 'lib/libtmp.php';
	//include 'lib/adminbackend.php';

	//$libtmp = new Templator("templ");
	$libdb  = new DBDriver;
	$libdb -> fn_connect(driver_mod, db_user, db_pass, db_name, db_host);

	function return_last_news_id()
	{
		global $libdb;
		$returned_id = $libdb -> fn_SelectFromTableWithCount('news',$data,1,'id');
		$returned_id['response'] = $returned_id[0];
		unset($returned_id[0]);
		header("Content-type: application/json");
		echo json_encode($returned_id);
	}

	function return_news_by_id($id)
	{
		global $libdb;

		if ($id <= 1) {
			header("Content-type: application/json");
			echo json_encode(array('response' => 'somethink'));
			exit;
		}

		for ($i=$id-1; $i > $id-4; $i--)
			if ($i > 1) {
				$data = array('id' => $i);
				$ret = $libdb -> fn_SelectFromTable('news',$data);
				$data = array('uid' => $ret[0]['preview_photo_uid']);
				$prew_photo_link = $libdb -> fn_SelectFromTable('photos', $data);
				$returned_news['response'][] = $ret[0];
			}

		header("Content-type: application/json");
		echo json_encode($returned_news);
	}

	function return_files_links_by_news_id($id)
	{
		global $libdb;
		$data = array('id' => $id);
		$news_data = $libdb -> fn_SelectFromTable('news',$data);
		$response = "error_null";
		
		if ($news_data[0]['album_id'] != '-1')
		{
			$data = array('id' => $news_data[0]['album_id']);
			$returned_data = $libdb -> fn_SelectFromTable('albums',$data);
			$returned_data = $returned_data[0];

			$data = array('uid' => $returned_data['photos_uid']);
			$returned_data = array();
			$returned_data['response'] = $libdb -> fn_SelectFromTable('photos', $data);

			header("Content-type: application/json");
			echo json_encode($returned_data);
		}
		else
		{
			header("Content-type: application/json");
			echo json_encode($response);
		}

	}

	function return_rasp()
	{
		return 0;
	}

	function return_last_news()
	{
		global $libdb;
		$data = array();
		$for_response = array();

		$returned_news = $libdb -> fn_SelectFromTableWithCount('news',$data,10,'id');

		for ($i=0; $i < count($returned_news); $i++) { 
			$data = array('uid' => $returned_news[$i]['preview_photo_uid']);
			$prew_photo_link = $libdb -> fn_SelectFromTable('photos', $data);

			$for_response['response'][$i] = $returned_news[$i];
			$for_response['response'][$i]['preview_photo_uid'] = $prew_photo_link[0]['file_name_preview'];
		}
		//$data = array('uid' => $returned_news[0]['preview_photo_uid']);
		//$prew_photo_link = $libdb -> fn_SelectFromTable('photos', $data);

		//$returned_news['response'] = $returned_news[0];
		//unset($returned_news[0]);
		header("Content-type: application/json");
		echo json_encode($for_response);
	}

	$response = "error_null";

	if (is_null($_REQUEST['req']))
	{
		header("Content-type: application/json");
		echo json_encode($response);
		exit;
	}

	if (!is_null($_REQUEST['req']))
	{
		switch ($_REQUEST['req']) {
			case 'get_last_news_id':
				return_last_news_id();
				break;

			case 'get_news_by_id':
				if (!is_null($_REQUEST['id']))
					return_news_by_id($_REQUEST['id']);
				else echo json_encode($response);
				break;

			case 'get_files_links_by_news_id':
				if (!is_null($_REQUEST['id']))
					return_files_links_by_news_id($_REQUEST['id']);
				else echo json_encode($response);
				break;

			case 'get_rasp':
				return_rasp();
				break;

			case 'get_last_news':
				return_last_news();
				break;
			
			default:
				echo json_encode($response);
				break;
		}
	}

?>