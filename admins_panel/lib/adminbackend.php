<?php
	
	function explodeExtension($string)
	{
		$result = explode(".", $string);
		return $result[1];
	}

	function getRandomString($count)
	{
		$random_mask = "qazplmxswijncdeuhbvfrghytQWEASDCXZVBNMKOPLFHGTRYU10569283475";
		for ($i=0; $i < $count; $i++)
			$test_string .= $random_mask[rand(1,strlen($random_mask)-1)];
		return $test_string;
	}

	function generateThumb($outputFilePath, $inputFilePath, $width)
	{
		//640px × 480px -- full
		//320px x 240px -- thumb

		//now as zaglushka
		//all pics will be generate as png with transparent, i think
		//as main block we need to use brodieja-thumb-generator

		//return getRandomString(8);

		if ($width == 640)
			$height = 480;
		else
			$height = 240;

		$rgb=0;
		$size = getimagesize($inputFilePath);
		// file-format
		$format = strtolower(substr($size['mime'], strpos($size['mime'], '/')+1));
		//определение функции соответственно типу файла
		$icfunc = "imagecreatefrom" . $format;
		//если нет такой функции прекращаем работу скрипта
		if (!function_exists($icfunc)) return 1;
		// пропорции
		$x_ratio = $width / $size[0];
		$y_ratio = $height / $size[1];
		$ratio       = min($x_ratio, $y_ratio);
		$use_x_ratio = ($x_ratio == $ratio);
		$new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);
		$new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);
		$new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width) / 2); 
		$new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);
		$img = imagecreatetruecolor($new_width,$new_height);
		imagefill($img, 0, 0, $rgb);
		$photo = $icfunc($inputFilePath);
		imagecopyresampled($img, $photo, 0, 0, 0, 0, $new_width, $new_height, $size[0], $size[1]);
		imagejpeg($img, $outputFilePath, 75);
		// Очищаем память после выполнения скрипта
		imagedestroy($img);
		imagedestroy($photo);

		return 0;
	}
	
	function echoTip($string)
	{
		echo "<div class='tip'>";
		echo "{$string}";
		echo "</div>";
	}

	function cache_menu_page($page_name, $template_name, $cache_name)
	{
		global $libtmp;

		//write header of page
		$handler_data = array(
			'templateName' => 'main_header.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array(
				'title' => $page_name,
				'menu_div' => file_get_contents("../res/templates/menu.html") )
		);

		$file_content = $libtmp -> QuickPageDraw($handler_data);

		//file_put_contents(templates_path."menu/".$template_name, "void");

		//write page body
		$handler_data = array(
			'templateName' => 'menu/'.$template_name,
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array()
		);

		$body_content = $libtmp -> QuickPageDraw($handler_data);

		$handler_data = array(
			'templateName' => 'main_body.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array(
				'news_responsible_html' => $body_content,
				'right_menu' => file_get_contents(templates_path."main_body_right_menu.html")
			)
		);

		$file_content .= $libtmp -> QuickPageDraw($handler_data);

		//write page footer
		$handler_data = array(
			'templateName' => 'main_footer.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array()
		);

		$file_content .= $libtmp -> QuickPageDraw($handler_data);
		// nginx return 502 -- TODO!!!
		
		try {
			//so, write in cache file
			$fp = fopen(script_path.cache_path.$cache_name, "w"); 
			//flock($fp, LOCK_UN); // Снятие блокировки
			//fflush($fp);
			fwrite($fp, $file_content);
			fclose($fp);

			echoTip("<b>'{$page_name}'</b> was cached in <b>'".cache_path.$cache_name."'</b>");
		
		} catch (Exception $e) {
			echoTip("error in caching \"{$page_name}\" ...");
		}

	}

	function show_selector_handler()
	{
		global $libtmp;

		$handler_data = array(
			'templateName' => 'adminpanel_main.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array(
				'title'     	=> 		'AdminPanel_v_0_2_5_341',
				'site_name'		=> 		'колледжа',
				'admin_panel_script_path' => admin_panel_script_path )
		);

		echo $libtmp -> QuickPageDraw($handler_data);
	}

	function show_template_editor($template_name)
	{
		// i save this function as thumb
		// so, in next refactoring i will drop it naher
	}

	function show_templates_editor()
	{
		// i save this function as thumb
		// so, in next refactoring i will drop it naher
	}

	function show_menu_editor()
	{
		// i save this function as thumb
		// so, in next refactoring i will drop it naher
	}

	function show_redraw_menu_template()
	{
		// i save this function as thumb
		// so, in next refactoring i will drop it naher
	}

	function fn_redraw_album_page($id)
	{
		// i save this function as thumb
		// so, in next refactoring i will drop it naher
	}

	function fn_redraw_index_page()
	{
		global $libtmp;
		$swit = 0;

		try {
			$js = file_get_contents("../res/templates/menu/menu.json");
			$test = json_decode($js, true);
			$swit = 1;
		} catch (Exception $e) {
			echoTip("Error reading menu.json");
			$swit = 0;
		}
		
		
		//deserialize, hm..
		//but, forming start divs

		if ($swit != 0)
		{
			$for_write = "<div class='menu-container'><div id='top-menu'><ul>";

			foreach ($test as $key => $value) {
				if (is_null($value['child']))
					$for_write .= "<li><a href='{$value['link']}'>{$key}</a></li>";
				else {
					//there pod-menus
					$for_write .= "<li><ul>";

					foreach ($value['child'] as $key0 => $value0) {
						// forming data
						$for_write .= "<li><a href='{$value0['link']}'>{$key0}</a></li>";
					}

					$for_write .= "</ul><a href='{$value['link']}'>{$key}</a></li>";
				}
			}

			//close divs
			$for_write .= "</ul></div></div>";

			try {
				//so, write in template file
				$fp = fopen(templates_path."menu.html", "w"); 
				flock($fp, LOCK_UN); // Снятие блокировки
				fflush($fp);
				fwrite($fp, $for_write);
				fclose($fp);

				$handler_data = array(
					'templateName' => 'index.html',
					'styleNames'   => array(),
					'jsNames'      => array(),
					'data'         => array(
						'title' => site_title,
						'menu' => file_get_contents("../res/templates/menu.html") )
				);
				$page_responsible = $libtmp -> QuickPageDraw($handler_data);
			} catch (Exception $e) {
				echoTip("Error in caching menu-template (menu.html)");
			}
			
			
			try {

				file_put_contents(cache_path.'index', $page_responsible);
				echoTip("INDEX PAGE REDRAW SUCCESSFUL!");

			} catch (Exception $e) {
				echoTip("Error in caching \"Главная\"");
			}
		}
	}

	function show_redraw_menu_templates()
	{
		global $libtmp;

		//print header of page
		$handler_data = array(
			'templateName' => 'adminpanel_header.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array(
				'title' => 'adminpanel',
				'back_addr' => admin_panel_script_path )
		);

		echo $libtmp -> QuickPageDraw($handler_data);

		echoTip("Здесь происходит перерисовка всех страничек сайта, что указаны в меню сайта.");
		echoTip("Здесь не будет перерисована главная страничка сайта /index.php/ и различные /rasp.php/ и для работы с альбомами.");
		echoTip("Они перерисовываются в других разделах административной панели.");

		//so, now lets work with our JSON menu
		$js = file_get_contents("../res/templates/menu/menu.json");
		$test = json_decode($js, true);

		foreach ($test as $key => $value) {
			if (!is_null($value['cache_name'])) 
				cache_menu_page($key, $value['template_name'], $value['cache_name']);
			
			if (!is_null($value['child']))
				foreach ($value['child'] as $key0 => $value0)
					if (!is_null($value['cache_name']))
						cache_menu_page($key0, $value0['template_name'], $value0['cache_name']);
		}

		fn_redraw_index_page();
	}

	function show_add_news()
	{
		global $libtmp;

		//print header of page
		$handler_data = array(
			'templateName' => 'adminpanel_header.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array(
				'title' => 'adminpanel',
				'back_addr' => admin_panel_script_path )
		);

		echo $libtmp -> QuickPageDraw($handler_data);

		echoTip("Это страничка для добавления новой новости...");
		echoTip("ВАЖНО : убрать галочку с пункта 'добавить альбом', если к новости нет фотографий");
		echoTip("Максимальное кол-во фотографий ограничено 15-изображениями за один раз (внутреннее ограничение движка на PHP на POST-запрос)");

		//so, the news-creator template..
		$handler_data = array(
			'templateName' => 'adminpanel_add_news_body.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array( "action" => admin_panel_script_path."?page=upload_news_post_data")
		);

		echo $libtmp -> QuickPageDraw($handler_data);
	}

	function show_add_news_uploading() 
	{
		global $libtmp, $libdb;
		//print header of page
		$handler_data = array(
			'templateName' => 'adminpanel_header.html',
			'styleNames'   => array(),
			'jsNames'      => array(),
			'data'         => array(
				'title' => 'adminpanel',
				'back_addr' => admin_panel_script_path )
		);
		echo $libtmp -> QuickPageDraw($handler_data);
		echo "</center>"; //fix fukken template

		//algorythm

		//if album-selector checked, create album, this album_id stored in news-table
		//then
		//store photos, generate thumbnails, write this data to DB
		//first photo, if it been, has uid, that store in news database, as preview to news
		//other photos, if album-selector checked, store to photos-table too, but with there store album_id
		//then
		//we had all data. Post it to news table. All right.
		//but, then draw new pages and redraw existed pages, heh.
		//and its an ALL RIGHT :33

		$current_max_news_id = $libdb -> fn_SelectFromTableWithCount('news',$data,1,'id');
		if (count($current_max_news_id) == 0)
			$news_id = 1;
		else
			$news_id = $current_max_news_id[0]['id'] + 1;

		$album_id = -1;
		if (!is_null($_REQUEST['create_album']))
		{
			//get known the album id...
			$data = array();
			$current_max_album_id = $libdb -> fn_SelectFromTableWithCount('albums',$data,1,'id');
			
			if (count($current_max_album_id) == 0)
				$album_id = 1;
			else
				$album_id = $current_max_album_id[0]['id'] + 1;
		}
		echoTip("debug : album_id = {$album_id}");

		//set count for block, that process images..
		//if 'create_album' not checked, then 1, else -- count of request

		if (!is_null($_REQUEST['create_album']))
			$process_count = count($_FILES['uploadfile']['name']);
		else
			$process_count = 1;

		$ran_str = getRandomString(8);
		echoTip("debug : random_string = {$ran_str}");

		$photo_meta_data_to_db = array();

		//verify load images..
		//if 'create_album' is not checked, and preview photo is not loaded, than $process_count is 0
		//if 'create_album' is checked, and preview photo is loaded, than all right
		//if 'create_album' is checked, but preview photo is not loaded, that processing block start from 1, not from 0 index of array
		//so, I can simply drop ERRORed images

		$current_photo_id = $libdb -> fn_SelectFromTableWithCount('photos',$data,1,'id');
		if (count($current_photo_id) == 0)
			$photo_id = 1;
		else
			$photo_id = $current_photo_id[0]['id'] + 1;

		$current_random_uid = getRandomString(16);
		$preview_random_uid = getRandomString(16);
		
		for ($i=0; $i < $process_count; $i++) { 
			if ($_FILES['uploadfile']['error'][$i] == 0)
			{	
				$rnd_str = getRandomString(8);
				$file_name = $rnd_str . "." . explodeExtension($_FILES['uploadfile']['name'][$i]);
				echoTip("debug : filename = ".full_picture_path.$file_name);

				if (is_uploaded_file($_FILES['uploadfile']['tmp_name'][$i]))
				{
			    	$old = $_FILES['uploadfile']['tmp_name'][$i];
			    	$new = full_picture_path.$file_name;
			        //copy($old,$new) or die("couldn't copy $old to $new: $php_errormsg");
			    	
			    	$rnd_str = getRandomString(8);
					$thumb_file_name = $rnd_str . "." . explodeExtension($_FILES['uploadfile']['name'][$i]);
					$new_thumb = thumb_picture_path.$thumb_file_name;

					generateThumb($new, $old, 640);
			    	generateThumb($new_thumb, $new, 320);

					echoTip("debug : thumb_filename = ".thumb_picture_path.$thumb_file_name);
			    }

			    //TODO препаринг данных для постинга в БД
			    //-id -uid -file_name_full -file_name_preview -description

			    if ($i == 0)
			    	$uid = $preview_random_uid; //non album photo
			    else
			    	$uid = $current_random_uid;

			    //640px × 480px -- full
				//320px x 240px -- thumb

			    $photo_meta_data_to_db[] = array(
			    	'id' => $photo_id,
			    	'uid' => $uid,
			    	'file_name_full' => $file_name,
			    	'file_name_preview' => $thumb_file_name,
			    	'description' => 'no_desc'
			    );

			    $photo_id++;

			} else {
				if ($i == 0) echoTip("<h1>WARNING</h1> Превью изображение не загружено.. Возможно, придется отдельно редактирвать новость.");
				if ($i != 0) 
					if (strlen($_FILES['uploadfile']['name'][$i]) > 0) 
						echoTip("Warning : Изображение с именем {$_FILES['uploadfile']['name'][$i]} не загружено в альбом, возможно, придется отдельно редактирвать альбом.");
			}
		}

		echoTip("//var_dump(photo_meta_data_to_db);");
		//var_dump($photo_meta_data_to_db);

		$album_meta_data_to_db = array(
			'id' => $album_id,
			'user_id' => 0,
			'view_count' => 0,
			'title' => $_REQUEST['news_album_name'],
			'photos_uid' => $current_random_uid,
			'main_photo_id' => $photo_meta_data_to_db[0]['id'],
		);

		echoTip("//var_dump(album_meta_data_to_db);");
		//var_dump($album_meta_data_to_db);

		//prepare_meta_data_for_news_to_post_to_db
		//so, lets collect data for post in DB..
		//-id -user_id -album_id -view_count -title -text -time -date -preview_photo_uid

		$news_meta_data_to_db = array(
			'id' => $news_id,
			'user_id' => 0,
			'album_id' => $album_id,
			'view_count' => 0,
			'title' => $_REQUEST['news_title'],
			'text' => $_REQUEST['news_text'],
			'time' => date("H:i:s"),
			'date' => date("d.m.y"),
			'preview_photo_uid' => $preview_random_uid
		);

		echoTip("//var_dump(news_meta_data_to_db);");
		//var_dump($news_meta_data_to_db);

		echoTip("debug : image_processing_count = {$process_count}");

		echoTip("//var_dump(_REQUEST);");
		//var_dump($_REQUEST);

		echoTip("//var_dump(_FILES);");
		//var_dump($_FILES);

		$libdb -> fn_PostToTable('news', $news_meta_data_to_db);

		if (!is_null($_REQUEST['create_album']))
			$libdb -> fn_PostToTable('albums', $album_meta_data_to_db);

		if ($process_count > 0)
			for ($i=0; $i < count($photo_meta_data_to_db); $i++) 
				$libdb -> fn_PostToTable('photos', $photo_meta_data_to_db[$i]);

		//after all redraw index-cache
		//todo -- draw/redraw news page; album-page; albums-pages (if it need, but i dont think that it true)
	
		fn_redraw_index_page();
		fn_redraw_album_page($album_meta_data_to_db['id']);
	}

	function show_menu_templates_editor()
	{
		
	}

?>