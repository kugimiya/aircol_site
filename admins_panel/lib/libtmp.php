<?php
	//this is tk.chimerus.templator, hello! :)

	class Templator 
	{
		var $_pageIndentificators;
		var $_pageData;
		var $_pagePrepared;
		var $_rawTemplates;

		public function Templator($test_var) 
		{
			$this -> _pageIndentificators = array();
			$this -> _pageData = array();
			$this -> _pagePrepared = array();
			$this -> _rawTemplates = array();
		}
		

		public function initPage($pageName, $pagePath)
		{
			$this -> _pageIndentificators[$pageName] = $pagePath;
			$this -> _pageData[$pageName] = array();
			$this -> _pagePrepared[$pageName] = "";
		}
		
		public function takeData($assocArray, $pageName)
		{
			$this -> _pageData[$pageName] = array_merge_recursive($assocArray);
		}

		public function loadTemplate($templateName)
		{
			$filename = script_path.templates_path.$templateName;

			if (is_null($this -> _rawTemplates[$templateName]))
				$this -> _rawTemplates[$templateName] = file_get_contents($filename);
		}
			
		public function connectStyle($styleName, $templateName)
		{
			$style_filename = script_path . styles_path . $styleName;
			$style_contents = "<style type='text/css'>\n" . file_get_contents($style_filename) . "\n</style>\n";
			$this -> _rawTemplates[$templateName] = ereg_replace(
				"<!--style:{$styleName}-->", 
				$style_contents,
				$this -> _rawTemplates[$templateName]
			);
		}

		public function connectJS($jsName, $templateName)
		{
			$js_filename = script_path . js_path . $jsName;
			$js_contents = "<script type='text/javascript'>\n" . file_get_contents($js_filename) . "\n</script>\n";
			$this -> _rawTemplates[$templateName] = ereg_replace(
				"<!--js:{$jsName}-->", 
				$js_contents,
				$this -> _rawTemplates[$templateName]
			);
		}

		public function connectToPage($pageName, $templateName)
		{
			$this -> _pagePrepared[$pageName] .= $this -> _rawTemplates[$templateName];
		}

		public function drawPage($pageName)
		{
			foreach ($this -> _pageData[$pageName] as $key => $value)
				$this -> _pagePrepared[$pageName] = ereg_replace(
					"%%{$key}%%", 
					$value, 
					$this -> _pagePrepared[$pageName]
				);

			$this -> _pageData[$pageName] = array();

			$file_point = fopen($this -> _pageIndentificators[$pageName] . $pageName, "w+");
			fputs($file_point, $this -> _pagePrepared[$pageName]);
			fclose($file_point);
		}

		public function addRawData($pageName, $rawData)
		{
			$this -> _pagePrepared[$pageName] .= $rawData;
		}

		public function preRendering($pageName)
		{
			foreach ($this -> _pageData[$pageName] as $key => $value)
				$this -> _pagePrepared[$pageName] = ereg_replace(
					"%%{$key}%%", 
					$value, 
					$this -> _pagePrepared[$pageName]
				);

			$this -> _pageData[$pageName] = array();
		}

		public function getPreRenderData($pageName)
		{
			return $this -> _pagePrepared[$pageName];
		}

		public function flushPage($pageName)
		{
			$this -> _pageIndentificators[$pageName] = "";
			$this -> _pageData[$pageName] = "";
			$this -> _pagePrepared[$pageName] = "";
		}

		//in future : flushTemplates (cause it can be very big...)

		public function QuickPageDraw($handlerData)
		{
			$pageName = rand(10,9000) . date('U') . 'quickRender';
			$this -> initPage($pageName, 'quickRender');
			$this -> loadTemplate($handlerData['templateName']);

			if (count($handlerData['styleNames']) > 0)
				for ($i=0; $i < count($handlerData['styleNames']); $i++) 
					$this -> connectStyle($handlerData['styleNames'][$i], $handlerData['templateName']);
				
			//TODO add for JS

			$this -> connectToPage($pageName, $handlerData['templateName']);
			$this -> takeData($handlerData['data'], $pageName);
			$this -> preRendering($pageName);
			$renderedPage = $this -> getPreRenderData($pageName);
			$this -> flushPage($pageName);

			return $renderedPage;
		}

	}
?>