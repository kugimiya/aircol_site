<?php	//this is kernel of db_module
	
/*		
	-- Connect to database
		fn_connect($driv_mod, $user, $pass, $db_name, $host)

	-- Create table in selected database
		fn_CreateTable($TableName, $rows)
			[x] rows array look like:
			[x] 'row name' => 'row type'
			
	-- Delete table in selected database
		fn_DeleteTable($TableName)

	-- Post new data to table in selected database
		fn_PostToTable($TableName, $dataToPostInTable)
			[x] data array look like:
			[x] 'row name' => 'safe row data'

	-- Delete data from table in selected database
		fn_DeletePost($TableName, $condition)
			[x] condition is array, and look like:
			[x] 'row name' => 'condition for row'
	
	-- Select rows from table in selected database
		fn_SelectFromTable($TableName, $condition)
			[x] condition is array, and look like:
			[x] 'row name' => 'condition for row'
			[x] if condition array is empty, returned all data
*/


    class DBDriver
    {
    	var $_pdo;

		function fn_slashes($text) { 
			return str_replace("'", "\'", $text);
			return $text;
		}

		function fn_connect($driv_mod, $user, $pass, $db_name, $host)
		{
			$dsn = "$driv_mod:host=$host;dbname=$db_name;";

			$opt = array(
			    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
			);

			$this -> _pdo = new PDO($dsn, $user, $pass, $opt);
		}

		function fn_makeQuery($query)
		{
			//var_dump($query);
			$prslk = $this -> _pdo -> query($query);
			return $prslk;
		}

		function fn_fetchSelectQuery($res)
		{
			$resd = array();

			while ($row = $res->fetch())
			    $resd[] = $row;
			
			return $resd;
		}

		function fn_getCondition($data)
		{
			$values = '';
			if (count($data) == 1) {
				foreach ($data as $key => $value)
					$values = "(`$key` = '$value')";
			} else {
				$values = '';
				foreach ($data as $key => $value)
					$values .= "(`{$key}` = '{fn_slashes($value)}') and ";
					$values = substr_replace ( $values, '', strlen($values) - 4 );
			}

			if (count($data) == 0) 
				$values = "NULL";

			return $values;
		}

		function fn_createDataBase($databaseName)
		{
			$query = "CREATE DATABASE `$databaseName`";
			$this -> fn_makeQuery($query);
		}

		function fn_CreateTable($TableName, $data)
		{
			$safeTableName = $this -> fn_slashes($TableName);
			
			$safeValues = "` (";
			foreach ($data as $key => $value) 
				$safeValues .= "`$key` $value, ";
			
			$safeValues = substr_replace ( $safeValues, ')', strlen($safeValues) - 2 );

			$query = "CREATE TABLE `".$safeTableName.$safeValues;

			//var_dump($query);
			$this -> fn_makeQuery($query);
		}

		function fn_DeleteTable($TableName)
		{
			$query = "DROP TABLE `$TableName`";
			//var_dump($query);
			$this -> fn_makeQuery($query);
		}

		function fn_PostToTable($TableName, $data)
		{
			$safeTableName = $this -> fn_slashes($TableName);

			$safeValues = "VALUES (";

			foreach ($data as $key => $value) 
				$safeValues .= "'{$this -> fn_slashes($value)}', ";

			$safeValues = substr_replace ( $safeValues, ')', strlen($safeValues) - 2 );

			$query = "INSERT INTO {$TableName} ".$safeValues;

			//var_dump($data);
			//var_dump($query);
			$this -> fn_makeQuery($query);
		}

		function fn_DeletePost($TableName, $data)
		{
			$query = "DELETE FROM `$TableName` WHERE ";
			// if length of $data array more, then one
			// 'where' block look like '(`name` = "fff") and (...) and (...)'

			$values = $this -> fn_getCondition($data);

			$query .= $values;

			//var_dump($query);
			$this -> fn_makeQuery($query);
		}

		function fn_UpdateRecord($TableName, $rowName, $value, $whereRowName, $whereValue)
		{
			$value = $this -> fn_slashes($value);

			//exmpl UPDATE `boards` SET `alias`=[value-1] WHERE 1

			$query = "UPDATE {$TableName} SET `{$rowName}`='{$value}' WHERE (`{$whereRowName}`='{$whereValue}')";
			
			//var_dump($query);

			$this -> fn_makeQuery($query);
		}

		function fn_SelectFromTable($TableName, $data)
		{
			$rows = '*';

			/*if (count($data) == 0) { 
				$rows = "*"; 
			} else {
				foreach ($data as $key => $value) 
					$rows .= "`$key`,";
				$rows = substr_replace ( $rows, '', strlen($rows) - 1 );
			}*/

			$query = "SELECT $rows FROM $TableName ";
			// if length of $data array more, then one
			// 'where' block look like '(`name` = "fff") and (...) and (...)'

			$values = $this -> fn_getCondition($data);

			if ($values != "NULL") {
				$values = "WHERE ".$values;
			} else {
				$values = "";
			}

			$query .= $values;

			//var_dump($query);
			$res = $this -> fn_makeQuery($query);

			return $this -> fn_fetchSelectQuery($res);
		}

		function fn_SelectFromTableWithCount($TableName,$data,$count,$order_column)
		{
			$rows = '*';

			/*if (count($data) == 0) { 
				$rows = "*"; 
			} else {
				foreach ($data as $key => $value) 
					$rows .= "`$key`,";
				$rows = substr_replace ( $rows, '', strlen($rows) - 1 );
			}*/
			//SELECT * FROM `messages` WHERE 1 ORDER BY `messages`.`timestamp` DESC LIMIT 15

			$query = "SELECT * FROM `$TableName` ";
			// if length of $data array more, then one
			// 'where' block look like '(`name` = "fff") and (...) and (...)'

			$values = $this -> fn_getCondition($data);

			if ($values != "NULL") {
				$values = "WHERE ".$values;
			} else {
				$values = "";
			}

			$query .= $values;

			$query .= " ORDER BY `{$order_column}` DESC LIMIT {$count}";

			//var_dump($query);
			$res = $this -> fn_makeQuery($query);

			return $this -> fn_fetchSelectQuery($res);
		}
	
	}

	/* delete this comment for YOBA-DEMO-IN-CONSOLE
	// #php file.php

	$table = date('U');

	$rows = array(
		'NAME'   => 'varchar(25)',
		'CIPHER' => 'int'
	);
	$db_worker -> fn_CreateTable($table, $rows);

	$dataToPostInTable = array(
		'NAME'   =>  'Vasya',
		'CIPHER' =>  '195322'
	);
	$db_worker -> fn_PostToTable($table, $dataToPostInTable);
	$db_worker -> fn_PostToTable($table, $dataToPostInTable);

	$condition = array();

	$result = $db_worker -> fn_SelectFromTable($table, $condition);
	var_dump($result);

	$condition = array(
		'NAME' => 'Vasya',
		'CIPHER' => '195322'
	);

	$db_worker -> fn_DeletePost($table, $condition);

	$db_worker -> fn_DeleteTable($table);

	var_dump('Test of var_dump'); 

	delete this comment */

?>