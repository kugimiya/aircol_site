<?php
	//info
	define("version", "0.1a");
	define("relise_name", "Tupyachka");

	//for work with DB
	define('driver_mod', 'mysql');
	define('db_user', 'root');
	define('db_pass', 'dev');
	define('db_host', 'localhost');
	define('db_name', '__dev');
	//tables
	//define('chat_table', 'messages');
	//define('last_visit_table', 'last_req');
	//define('fail_login_table', 'log_table');
	//define('notify_table', 'notify');
	//define('auth_table', 'auth_keys');
	
	//for work with --noname_smarty--
	define("script_path", "/var/www/air_site/aircol_site/admins_panel/");
	define("domain_path", "/");
	define("files_path", "../res/files/");
	define("templates_path", "../res/templates/");
	define("menu_templates_path", "../res/templates/menu/");
	define("styles_path", "../res/styles/");
	define("js_path", "../res/js/");
	define("cache_path", "../res/cache/");
	define("menu_path", "../res/templates/menu/");

	//some_data
	define("admin_panel_script_path", "admin.php");
	define("full_picture_path", '../res/files/img_full/');
	define("thumb_picture_path", '../res/files/img_thumb/');
	define("site_title", "Ульяновский Авиационный Колледж");
?>