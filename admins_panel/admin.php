<?php
	ini_set('display_errors', 0);
	ini_set('max_file_uploads', 17);
	ini_set('xdebug.show_local_vars', 1);
	//session_start();
	
	include 'lib/config.php';
	include 'lib/libdb.php';
	include 'lib/libtmp.php';
	include 'lib/adminbackend.php';

	$libtmp = new Templator("templ");
	$libdb  = new DBDriver;
	$libdb -> fn_connect(driver_mod, db_user, db_pass, db_name, db_host);

	//menu template like JSON document
	//so, now try to read it and output

	/*$js = file_get_contents("res/templates/menu.json");
	$test = json_decode($js);
	var_dump($test);*/

	if (is_null($_REQUEST['page']))
		show_selector_handler();
	else 
	{
		if ($_REQUEST['page'] == 'redraw_menu_templates') {
			show_redraw_menu_templates();
			exit;
		}		

		if ($_REQUEST['page'] == 'add_news') {
			show_add_news();
			exit;
		}

		if ($_REQUEST['page'] == 'upload_news_post_data') {
			show_add_news_uploading();
			exit;
		}
	}

?>