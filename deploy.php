<?php
	
	ini_set('display_errors', 1);
	
	include 'admins_panel/lib/config.php';
	include 'admins_panel/lib/libdb.php';

	$libdb  = new DBDriver;
	$libdb -> fn_connect(driver_mod, db_user, db_pass, db_name, db_host);

	//create tables
	//TODO

	echo "Now is creating tables<br>";

	echo "Users table<br>";
	//users tables
	$users_rows = array(
		'id' => 'int',
		'login_hash' => 'varchar(64)',
		'pass_hash' => 'varchar(64)',
		'access_level' => 'int'
	);
	$libdb -> fn_CreateTable('users', $users_rows);


	echo "News table<br>";
	//users tables
	$news_rows = array(
		'id' => 'int',
		'user_id' => 'int',
		'album_id' => 'int',
		'view_count' => 'int',
		'title' => 'varchar(256)',
		'text' => 'varchar(65565)',
		'time' => 'varchar(12)',
		'date' => 'varchar(12)',
		'preview_photo_uid' => 'varchar(64)'
	);
	$libdb -> fn_CreateTable('news', $news_rows);


	echo "Albums table<br>";
	//users tables
	$albums_rows = array(
		'id' => 'int',
		'user_id' => 'int',
		'view_count' => 'int',
		'title' => 'varchar(256)',
		'photos_uid' => 'varchar(64)',
		'main_photo_id' => 'varchar(64)'
	);
	$libdb -> fn_CreateTable('albums', $albums_rows);


	echo "Photos table<br>";
	//users tables
	$albums_rows = array(
		'id' => 'int',
		'uid' => 'varchar(64)',
		'file_name_full' => 'varchar(256)',
		'file_name_preview' => 'varchar(256)',
		'description' => 'varchar(256)'
	);
	$libdb -> fn_CreateTable('photos', $albums_rows);

?>